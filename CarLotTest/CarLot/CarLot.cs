﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarLot
{
    public class CarLot
    {
        List<Car> cars = new List<Car>();

        public void AddCar()
        {
            Console.Clear();
            Car newCar = new Car();
            Console.WriteLine("1: Pinto");
            Console.WriteLine("2: Semi");
            Console.WriteLine("3: Funny Car");
            Console.WriteLine("Please select car type: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    newCar.carType = CarType.Pinto;
                    newCar.fuelType = FuelType.Gas;
                    break;
                case 2:
                    newCar.carType = CarType.Semi;
                    newCar.fuelType = FuelType.Diesel;
                    break;
                case 3:
                    newCar.carType = CarType.Funny;
                    newCar.fuelType = FuelType.Methanol;
                    break;
            }

            cars.Add(newCar);
        }
    }
}
