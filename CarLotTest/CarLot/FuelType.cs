﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarLot
{
    public enum FuelType
    {
        Gas,
        Diesel,
        Methanol
    }
}
