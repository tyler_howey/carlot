﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarLot
{
    

    public class ManageMenu
    {
        CarLot _lot = new CarLot();


        public void ShowMenu()
        {
            Console.WriteLine("1: Add car");
            Console.WriteLine("2: Drive car");
            Console.WriteLine("3: Fuel car");

            switch (int.Parse(Console.ReadLine()))
            {
                case 1:
                    _lot.AddCar();
                    break;
            }
        }
    }

    
}
