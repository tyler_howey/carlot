﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarLot
{
    public class Car : ICarType
    {
        private int _fuelLevel { get; set; }
        private double _price { get; set; }

        public CarType carType { get; set; }
        public FuelType fuelType { get; set; }

        public Car()
        {

            _price = 20000;
            _fuelLevel = 14;
        }

        private void FuelUp()
        {
            foreach (var i in Enum.GetValues(typeof(ICarType)))
            {
                Console.WriteLine(i + ":" + i.ToString());
            }
            Console.WriteLine("Please select fuel type: ");
            int choice = int.Parse(Console.ReadLine());
            if (choice != (int)fuelType)
            {
                Console.WriteLine("You have used the wrong fuel and the car will no longer start.");
                _fuelLevel = -1;
            }
            else
            {
                Console.WriteLine("You're all fueled up!");
                _fuelLevel = 14;
            }
            Console.ReadKey();
        }

        private void Sale()
        {
            _price -= (_price * 0.2);
        }

        private void Honk()
        {
            Console.WriteLine("Beep");
            Console.ReadKey();
        }

        private void Drive()
        {
            if(_fuelLevel < 3)
            {
                _fuelLevel = 0;
                Console.WriteLine("Oh No, you are out of fuel.");
            }
            else
            {
                if(carType == CarType.Funny)
                {
                    _fuelLevel -= 14;
                }
                else
                {
                    _fuelLevel -= 3;
                }
                Console.WriteLine("Zoom");
            }
            Console.ReadKey();
        }
    }
}
